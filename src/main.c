#include <zephyr/kernel.h>
#include <zephyr/device.h>
#include <zephyr/devicetree.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/logging/log.h>
#include <zephyr/drivers/adc.h> 
#include <zephyr/smf.h>
#include <zephyr/drivers/pwm.h>
#include <nrfx_power.h> 
#include "pressure.h"

#include <zephyr/bluetooth/bluetooth.h>
#include <zephyr/bluetooth/uuid.h>
#include <zephyr/bluetooth/gatt.h>
#include <zephyr/bluetooth/hci.h> // host controller interface
#include <zephyr/bluetooth/services/bas.h>  // battery service GATT
#include <zephyr/settings/settings.h>

LOG_MODULE_REGISTER(main, LOG_LEVEL_DBG);

// Define some macros to use some Zephyr macros to help read the DT configuration 
#define ADC_DT_SPEC_GET_BY_ALIAS(adc_alias)                    \
{                                                              \
    .dev = DEVICE_DT_GET(DT_PARENT(DT_ALIAS(adc_alias))),      \
    .channel_id = DT_REG_ADDR(DT_ALIAS(adc_alias)),            \
    ADC_CHANNEL_CFG_FROM_DT_NODE(DT_ALIAS(adc_alias))          \
}                             

// Define PWM structs based on DT aliases
static const struct pwm_dt_spec vpp100_brightness_control = PWM_DT_SPEC_GET(DT_ALIAS(pwm100));
static const struct pwm_dt_spec vpp500_brightness_control = PWM_DT_SPEC_GET(DT_ALIAS(pwm500));

// Intialize the ADC struct to store all the DT parameters 
static const struct adc_dt_spec vpp100 = ADC_DT_SPEC_GET_BY_ALIAS(vpp100);
static const struct adc_dt_spec vpp500 = ADC_DT_SPEC_GET_BY_ALIAS(vpp500);
static const struct adc_dt_spec battery = ADC_DT_SPEC_GET_BY_ALIAS(battery);

// Honeywell_mpr is a default device in the Zephyr ecosystem
const struct device *const pressure_in = DEVICE_DT_GET_ONE(honeywell_mpr); 

// Defining the led GPIO pins and their nodes from the device tree 
#define LED0_NODE DT_ALIAS(action100)
#define LED1_NODE DT_ALIAS(action500)
#define LED2_NODE DT_ALIAS(error)
#define LED3_NODE DT_ALIAS(heartbeat)

static const struct gpio_dt_spec action100_led = GPIO_DT_SPEC_GET(LED0_NODE,gpios);
static const struct gpio_dt_spec action500_led = GPIO_DT_SPEC_GET(LED1_NODE,gpios);
static const struct gpio_dt_spec error_led = GPIO_DT_SPEC_GET(LED2_NODE,gpios);
static const struct gpio_dt_spec heartbeat_led = GPIO_DT_SPEC_GET(LED3_NODE,gpios);

// Defining the buttons from the device tree
#define SW0_NODE DT_ALIAS(sw0)
#define SW1_NODE DT_ALIAS(sw1)

static const struct gpio_dt_spec save = GPIO_DT_SPEC_GET(DT_ALIAS(save),gpios);
static const struct gpio_dt_spec send = GPIO_DT_SPEC_GET(DT_ALIAS(bluetooth),gpios);

// Define structure to hold button callback info
static struct gpio_callback save_cb;
static struct gpio_callback send_cb;

// Timer function declarations
void heartbeat_start (struct k_timer *heartbeat_timer);
void error_start (struct k_timer *error_timer);
void error_stop (struct k_timer *error_timer);
void read_100hz_start (struct k_timer *read_100hz_timer);
void read_500hz_start (struct k_timer *read_500hz_timer);
void battery_level (struct k_timer *battery_timer);
void pressure_start (struct k_timer *pressure_timer);

// Defining timers 
K_TIMER_DEFINE(heartbeat_timer, heartbeat_start, NULL);
K_TIMER_DEFINE(error_timer, error_start, error_stop);
K_TIMER_DEFINE(read_100hz_timer, read_100hz_start, NULL);
K_TIMER_DEFINE(read_500hz_timer, read_500hz_start, NULL);
K_TIMER_DEFINE(battery_timer, battery_level, NULL);
K_TIMER_DEFINE(pressure_timer, pressure_start, NULL);


// Handler function declarations
void heartbeat_timer_handler(struct k_work *heartbeat_timer_work);
void error_start_handler(struct k_work *error_start_work);
void error_stop_handler(struct k_work *error_stop_work);
void save_handler(struct k_work *save_work);
void send_handler(struct k_work *send_work);
void read_100hz_start_handler(struct k_work *read_100hz_start_work);
void read_500hz_handler(struct k_work *read_500hz_work);
void battery_handler(struct k_work *battery_work);
void pressure_handler(struct k_work *pressure_work);

// Defining work
K_WORK_DEFINE(heartbeat_timer_work, heartbeat_timer_handler);
K_WORK_DEFINE(error_start_work, error_start_handler);  
K_WORK_DEFINE(error_stop_work, error_stop_handler);
K_WORK_DEFINE(save_work, save_handler);
K_WORK_DEFINE(send_work, send_handler);
K_WORK_DEFINE(read_100hz_start_work, read_100hz_start_handler);
K_WORK_DEFINE(read_500hz_work, read_500hz_handler);
K_WORK_DEFINE(battery_work, battery_handler);
K_WORK_DEFINE(pressure_work, pressure_handler);

// MACROS
#define LED_ON_TIME_MS 1000
#define VOLTAGE_RANGE 3.3
#define NUM_STEPS 20
#define INTERVALS 40
#define MEASUREMENT_DELAY_MS 1000  
#define OVERSAMPLE 10  
#define MAX_BRIGHTNESS 1000000

// ADC value variables
int32_t val_mv0;  // 100Hz
int32_t val_mv1;  // 500Hz
int32_t val_mv2;  // battery
int16_t buf0;
int16_t buf1;
int16_t buf2;

// VBUS 
bool usbregstatus; 

// Pressure sensor 
static int32_t atm_pressure_kPa;

// 100Hz variables
int32_t sine_value_0[10];
int32_t min_value_0;
int32_t max_value_0;
int32_t vpp_0;
int32_t sum_0;
int32_t average_0; 

int32_t step_counter_0 = 0; 
int wave_counter_0 = 0; 
float LED1factor = 0; 
float brightness_100hz = 0; 

// 500Hz variables
int16_t sine_value_1[10];
int32_t min_value_1;
int32_t max_value_1;
int32_t vpp_1;
int32_t sum_1;
int32_t average_1; 

int32_t step_counter_1 = 0; 
int wave_counter_1 = 0; 
float LED2factor = 0; 
float brightness_500hz = 0; 

// For configuring the buttons and adding callbacks
int ret;

int16_t data_array[3];
int pressure_array[10];
int pressure_counter;
int sum;
int pressure_average;

// Reading data from ADC buffer
    struct adc_sequence vpp100_sequence = 
    {
        .buffer = &buf0,
        .buffer_size = sizeof(buf0), // bytes
    };

    struct adc_sequence vpp500_sequence = 
    {
        .buffer = &buf1,
        .buffer_size = sizeof(buf1), // bytes
    };

    struct adc_sequence battery_sequence = 
    {
        .buffer = &buf2,
        .buffer_size = sizeof(buf2), // bytes
    };

/* Define macros for UUIDs of the Remote Service
   Project ID: 001 (3rd entry)
   MFG ID = 0x01FF (4th entry)
*/
#define BT_UUID_REMOTE_SERV_VAL \
        BT_UUID_128_ENCODE(0xe9ea0000, 0xe19b, 0x0001, 0x01FF, 0xc7907585fc48)
#define BT_UUID_REMOTE_SERVICE          BT_UUID_DECLARE_128(BT_UUID_REMOTE_SERV_VAL)

/* UUID of the Data Characteristic */
#define BT_UUID_REMOTE_DATA_CHRC_VAL \
        BT_UUID_128_ENCODE(0xe9ea0001, 0xe19b, 0x0001, 0x01FF, 0xc7907585fc48)
#define BT_UUID_REMOTE_DATA_CHRC        BT_UUID_DECLARE_128(BT_UUID_REMOTE_DATA_CHRC_VAL)

/* UUID of the Message Characteristic */
#define BT_UUID_REMOTE_MESSAGE_CHRC_VAL \
        BT_UUID_128_ENCODE(0xe9ea0002, 0xe19b, 0x0001, 0x01FF, 0xc7907585fc48)
#define BT_UUID_REMOTE_MESSAGE_CHRC     BT_UUID_DECLARE_128(BT_UUID_REMOTE_MESSAGE_CHRC_VAL) 

#define DEVICE_NAME CONFIG_BT_DEVICE_NAME
#define DEVICE_NAME_LEN (sizeof(DEVICE_NAME)-1)
#define BLE_DATA_POINTS 600 // limited by MTU

/***********************  STATE ENUM  *****************************/ 
static const struct smf_state device_states[];
enum device_states {init, read, action, error, connect};

/* User defined object */
struct s_object {
    struct smf_ctx ctx;
} s_obj;

/********* enumeration to keep track of the state of the notifications ***************/
enum bt_data_notifications_enabled {
    BT_DATA_NOTIFICATIONS_ENABLED,
    BT_DATA_NOTIFICATIONS_DISABLED,
};
enum bt_data_notifications_enabled notifications_enabled;

/***********************  READ 100HZ START  *****************************/ 
void read_100hz_start(struct k_timer *read_100hz_timer)
{
    k_work_submit(&read_100hz_start_work);
}

void read_100hz_start_handler(struct k_work *read_100hz_start_work) 
{
    // Read adc signal in channel 0 .
    LOG_INF("Measuring %s (channel %d)... ", vpp100.dev->name, vpp100.channel_id);

    (void)adc_sequence_init_dt(&vpp100, &vpp100_sequence);

    int ret0 = adc_read(vpp100.dev, &vpp100_sequence);
    if (ret0 < 0) {
        LOG_ERR("Could not read (%d)", ret0);
    } else {
        LOG_DBG("Raw ADC0 Buffer: %d", buf0);
    }   
    // Convert to voltage 
    val_mv0 = buf0;
    ret0 = adc_raw_to_millivolts_dt(&vpp100, &val_mv0); 
    if (ret0 < 0) {
        LOG_ERR("Buffer cannot be converted to mV; returning raw buffer value.");
    } else {
        LOG_INF("ADC0 Value (mV): %d", val_mv0);
    }

    // Store the adc data in array
    sine_value_0[step_counter_0] = val_mv0;
    step_counter_0 ++;

    // Sort to find the max and the min once the array is full
    if (step_counter_0 == 10){
        for (int i = 0; i<10; i++){
            if (sine_value_0[i] < min_value_0){
                min_value_0 = sine_value_0[i];
            }
            if (sine_value_0[i] > max_value_0){
                max_value_0 = sine_value_0[i];
            }
        }
        LOG_INF("Step Counter: %d", step_counter_0);

        // Calculate peak to peak voltage
        vpp_0 = max_value_0 - min_value_0;
        LOG_INF("VPP: %d", vpp_0);
        LOG_INF("MAX VALUE: %d", max_value_0);
        LOG_INF("MIN VALUE: %d", min_value_0);
        
        // Calculate the sum, +1 the wave counter, reset the step counter 
        sum_0 = sum_0 + vpp_0;
        LOG_INF("SUM: %d", sum_0);
        wave_counter_0 ++;
        step_counter_0 = 0;

        if (wave_counter_0 == 100) {
            average_0 = sum_0/100; 
            LOG_INF("AVERAGE: %d", average_0);
            smf_set_state(SMF_CTX(&s_obj), &device_states[action]);
        }
    }
}

/***********************  READ 500HZ START  *****************************/ 
void read_500hz_start(struct k_timer *read_500hz_timer)
{
    k_work_submit(&read_500hz_work);
}

void read_500hz_handler(struct k_work *read_500hz_work) 
{
    // Read adc signal in channel 1
    LOG_INF("Measuring %s (channel %d)... ", vpp500.dev->name, vpp500.channel_id);
    
    (void)adc_sequence_init_dt(&vpp500, &vpp500_sequence );
    
    int ret1 = adc_read(vpp500.dev, &vpp500_sequence);
    if (ret1 < 0) {
        LOG_ERR("Could not read (%d)", ret1);
    } else {
        LOG_DBG("Raw ADC1 Buffer: %d", buf1);
    }

    // Convert to voltage
    val_mv1 = buf1;
    ret1 = adc_raw_to_millivolts_dt(&vpp500, &val_mv1); // remember that the vadc struct containts all the DT parameters
    if (ret1 < 0) {
        LOG_ERR("Buffer cannot be converted to mV; returning raw buffer value.");
    } else {
        LOG_INF("ADC1 Value (mV): %d", val_mv1);
    }

    // Store adc data in array
    sine_value_1[step_counter_1] = val_mv1;
    step_counter_1 ++;

    // Find the min and max by using for loop
    if (step_counter_1 == 9){
        for (int i = 0; i<10; i++){
            if (sine_value_1[i] < min_value_1){
                min_value_1 = sine_value_1[i];
            }
            if (sine_value_1[i] > max_value_1){
                max_value_1 = sine_value_1[i];
            }
        }
        vpp_1 = max_value_1 - min_value_1;
        LOG_INF("VPP_500: %d", vpp_1);
        LOG_INF("MAX VALUE 500: %d", max_value_1);
        LOG_INF("MIN VALUE 500: %d", min_value_1);
        sum_1 = sum_1 + vpp_1;
        LOG_INF("SUM: %d", sum_1);
        wave_counter_1++;
        if (wave_counter_1 == 100) {
            average_1 = sum_1/100; 
            LOG_INF("AVERAGE: %d", average_1);
            smf_set_state(SMF_CTX(&s_obj), &device_states[action]);
        }
    }

}

/***********************  BATTERY START  *****************************/ 
void battery_level(struct k_timer *battery_timer)
{
    k_work_submit(&battery_work);
}

void battery_handler(struct k_work *battery_work) 
{
    // Read adc signal in channel 2
    LOG_INF("Measuring %s (channel %d)... ", battery.dev->name, battery.channel_id);
    
    (void)adc_sequence_init_dt(&battery, &battery_sequence);
    
    int ret2 = adc_read(battery.dev, &battery_sequence);
    if (ret2 < 0) {
        LOG_ERR("Could not read ret2 (%d)", ret2);
    } else {
        LOG_DBG("Raw ADC2 Buffer: %d", buf2);
    }

    // Convert to voltage
    val_mv2 = buf2;
    ret2 = adc_raw_to_millivolts_dt(&battery, &val_mv2); // remember that the vadc struct containts all the DT parameters
    if (ret2 < 0) {
        LOG_ERR("Buffer cannot be converted to mV; returning raw buffer value.");
    } else {
        LOG_INF("ADC2 Value (mV): %d", val_mv2);
    }
}

/***********************  HB START  *****************************/ 
void heartbeat_start(struct k_timer *heartbeat_timer)
{
    k_work_submit(&heartbeat_timer_work);
}

void heartbeat_timer_handler(struct k_work *heartbeat_timer_work) 
{
    gpio_pin_toggle_dt(&heartbeat_led);
}

/***********************  ERROR START  *****************************/ 
void error_start(struct k_timer *error_timer)
{
    k_work_submit(&error_start_work);
}

void error_start_handler(struct k_work *error_start_work) 
{
    gpio_pin_toggle_dt(&error_led);
}

/***********************  ERROR STOP  *****************************/ 
void error_stop(struct k_timer *error_timer)
{
    k_work_submit(&error_stop_work);
}

void error_stop_handler(struct k_work *error_stop_work) 
{
    gpio_pin_set_dt(&error_led, 0);
}

/***********************  SAVE  Button *****************************/ 
void save_pressed(const struct device *dev, struct gpio_callback *cb, uint32_t pins)
{
    k_work_submit(&save_work);
}

void save_handler(struct k_work *save_work)
{
    
    // Save pressure and adc data in array 
    int16_t led1_brightness = average_0; 
    int16_t led2_brightness = average_1; 
    int16_t atmospheric_pressure = atm_pressure_kPa; // maybe times 100

    data_array[0] = (led1_brightness);
    data_array[1] = (led2_brightness);
    data_array[2] = (atmospheric_pressure);

    LOG_INF("LED1 Brightness: (%d)", led1_brightness);
    LOG_INF("LED2 Brightness: (%d)", led2_brightness);
    LOG_INF("Atmospheric pressure: (%d)", atmospheric_pressure);
    

    smf_set_state(SMF_CTX(&s_obj), &device_states[read]);
}

/***********************  SEND  *****************************/ 
void send_pressed(const struct device *dev, struct gpio_callback *cb, uint32_t pins)
{
    k_work_submit(&send_work);
}

void send_handler(struct k_work *send_work)
{
    smf_set_state(SMF_CTX(&s_obj), &device_states[connect]);
}

/******* Pressure **********/

void pressure_start(struct k_timer *pressure_timer) {

    k_work_submit(&pressure_work);

}

void pressure_handler(struct k_work *pressure_work) 
{
    pressure_array[pressure_counter] = atm_pressure_kPa;
    pressure_counter ++;
    if (pressure_counter == 10) {
        for (int i=0; i < 10; i++){
            sum += pressure_array[i];
        }
        pressure_average = sum / 10;
        k_timer_stop(&pressure_timer);
    }
}

/*************** declare struct of the BLE remove service callbacks *****************/
struct bt_remote_srv_cb {
    void (*notif_changed)(enum bt_data_notifications_enabled status);
    void (*data_rx)(struct bt_conn *conn, const uint8_t *const data, uint16_t len);
}; 
static struct bt_remote_srv_cb remote_service_callbacks;

// blocking thread semaphore to wait for BLE to initialize
static K_SEM_DEFINE(bt_init_ok, 1, 1);

/* Advertising data */
static const struct bt_data ad[] = {
    BT_DATA_BYTES(BT_DATA_FLAGS, (BT_LE_AD_GENERAL | BT_LE_AD_NO_BREDR)),
    BT_DATA(BT_DATA_NAME_COMPLETE, DEVICE_NAME, DEVICE_NAME_LEN)
};

/* Scan response data */
static const struct bt_data sd[] = {
    BT_DATA_BYTES(BT_DATA_UUID128_ALL, BT_UUID_REMOTE_SERV_VAL), 
};

/* Function Declarations */
static ssize_t read_data_cb(struct bt_conn *conn, const struct bt_gatt_attr *attr, void *buf, uint16_t len, uint16_t offset);
static ssize_t read_data_cb1(struct bt_conn *conn, const struct bt_gatt_attr *attr, void *buf, uint16_t len, uint16_t offset);
void data_ccc_cfg_changed_cb(const struct bt_gatt_attr *attr, uint16_t value);
static ssize_t on_write(struct bt_conn *conn, const struct bt_gatt_attr *attr, const void *buf, uint16_t len, uint16_t offset, uint8_t flags);
void bluetooth_set_battery_level(int level, int nominal_batt_mv);
uint8_t bluetooth_get_battery_level(void);

/* Setup BLE Services */
BT_GATT_SERVICE_DEFINE(remote_srv,
    BT_GATT_PRIMARY_SERVICE(BT_UUID_REMOTE_SERVICE),
    BT_GATT_CHARACTERISTIC(BT_UUID_REMOTE_DATA_CHRC, BT_GATT_CHRC_READ | BT_GATT_CHRC_NOTIFY, BT_GATT_PERM_READ, read_data_cb, NULL, NULL),
    BT_GATT_CHARACTERISTIC(BT_UUID_REMOTE_DATA_CHRC, BT_GATT_CHRC_READ | BT_GATT_CHRC_NOTIFY, BT_GATT_PERM_READ, read_data_cb1, NULL, NULL),
    BT_GATT_CCC(data_ccc_cfg_changed_cb, BT_GATT_PERM_READ | BT_GATT_PERM_WRITE),   
    BT_GATT_CHARACTERISTIC(BT_UUID_REMOTE_MESSAGE_CHRC, BT_GATT_CHRC_WRITE_WITHOUT_RESP, BT_GATT_PERM_WRITE, NULL, on_write, NULL),
);

/* BLE Callback Functions */
void data_ccc_cfg_changed_cb(const struct bt_gatt_attr *attr, uint16_t value) {
    bool notif_enabled = (value == BT_GATT_CCC_NOTIFY);
    LOG_INF("Notifications: %s", notif_enabled? "enabled":"disabled");

    notifications_enabled = notif_enabled? BT_DATA_NOTIFICATIONS_ENABLED:BT_DATA_NOTIFICATIONS_DISABLED;

    if (remote_service_callbacks.notif_changed) {
        remote_service_callbacks.notif_changed(notifications_enabled);
    }
}

static ssize_t read_data_cb(struct bt_conn *conn, const struct bt_gatt_attr *attr, void *buf, uint16_t len, uint16_t offset) {
    return bt_gatt_attr_read(conn, attr, buf, len, offset, &sine_value_0, sizeof(sine_value_0));
}

static ssize_t read_data_cb1(struct bt_conn *conn, const struct bt_gatt_attr *attr, void *buf, uint16_t len, uint16_t offset) {
    return bt_gatt_attr_read(conn, attr, buf, len, offset, &sine_value_1, sizeof(sine_value_1));
}

static ssize_t on_write(struct bt_conn *conn, const struct bt_gatt_attr *attr, const void *buf, uint16_t len, uint16_t offset, uint8_t flags) {
    LOG_INF("Received data, handle %d, conn %p", attr->handle, (void *)conn);
    
    if (remote_service_callbacks.data_rx) {
        remote_service_callbacks.data_rx(conn, buf, len);
    }
    return len;
}

void on_sent(struct bt_conn *conn, void *user_data) {
    ARG_UNUSED(user_data);
    LOG_INF("Notification sent on connection %p", (void *)conn);
}

void bt_ready(int ret) {
    if (ret) {
        LOG_ERR("bt_enable returned %d", ret);
    }

    // release the thread once initialized
    k_sem_give(&bt_init_ok);
}

int send_data_notification(struct bt_conn *conn, uint8_t *value, uint16_t length) {
    int ret = 0;

    struct bt_gatt_notify_params params = {0};
    const struct bt_gatt_attr *attr = &remote_srv.attrs[2];

    params.attr = attr;
    params.data = &value;
    params.len = length;
    params.func = on_sent;

    ret = bt_gatt_notify_cb(conn, &params);

    return ret;
}

/* Initialize the BLE Connection */
int bluetooth_init(struct bt_conn_cb *bt_cb, struct bt_remote_srv_cb *remote_cb) {
    LOG_INF("Initializing Bluetooth");

    if ((bt_cb == NULL) | (remote_cb == NULL)) {
        return -NRFX_ERROR_NULL;
    }
    bt_conn_cb_register(bt_cb);
    remote_service_callbacks.notif_changed = remote_cb->notif_changed;
    remote_service_callbacks.data_rx = remote_cb->data_rx;

    int ret = bt_enable(bt_ready);
    if (ret) {
        LOG_ERR("bt_enable returned %d", ret);
        return ret;
    }

    // hold the thread until Bluetooth is initialized
    k_sem_take(&bt_init_ok, K_FOREVER);

    if (IS_ENABLED(CONFIG_BT_SETTINGS)) {
        settings_load();
    }

    ret = bt_le_adv_start(BT_LE_ADV_CONN, ad, ARRAY_SIZE(ad), sd, ARRAY_SIZE(sd));
    if (ret) {
        LOG_ERR("Could not start advertising (ret = %d)", ret);
        return ret;
    }

    return ret;

}

/* Function Declarations */
static struct bt_conn *current_conn;
void on_connected(struct bt_conn *conn, uint8_t ret);
void on_disconnected(struct bt_conn *conn, uint8_t reason);
void on_notif_changed(enum bt_data_notifications_enabled status);
void on_data_rx(struct bt_conn *conn, const uint8_t *const data, uint16_t len);

/* Setup Callbacks */
struct bt_conn_cb bluetooth_callbacks = {
    .connected = on_connected,
    .disconnected = on_disconnected,
};

static struct bt_remote_srv_cb remote_service_callbacks = {
    .notif_changed = on_notif_changed,
    .data_rx = on_data_rx,
};

void on_data_rx(struct bt_conn *conn, const uint8_t *const data, uint16_t len) {
    uint8_t temp_str[len+1];
    memcpy(temp_str, data, len);
    temp_str[len] = 0x00; // manually append NULL character at the end

    LOG_INF("BT received data on conn %p. Len: %d", (void *)conn, len);
    LOG_INF("Data: %s", temp_str);
}

void on_connected(struct bt_conn *conn, uint8_t ret) {
    if (ret) { LOG_ERR("Connection error: %d", ret); }
    LOG_INF("BT connected");
    current_conn = bt_conn_ref(conn);
}

void on_disconnected(struct bt_conn *conn, uint8_t reason) {
    LOG_INF("BT disconnected (reason: %d)", reason);
    if (current_conn) {
        bt_conn_unref(current_conn);
        current_conn = NULL;
    }
}

void on_notif_changed(enum bt_data_notifications_enabled status) {
    if (status == BT_DATA_NOTIFICATIONS_ENABLED) {
        LOG_INF("BT notifications enabled");
    }
    else {
        LOG_INF("BT notifications disabled");
    }
}

/*****************  INIT STATE  *******************/ 
static int init_entry(void *o)
{
    int32_t ret;                                                        
    // Checking the leds
    if (!device_is_ready(heartbeat_led.port)) {
        return -1;
    }
    if (!device_is_ready(action100_led.port)) {
        return -1;
    }
    if (!device_is_ready(action500_led.port)) {
        return -1;
    }
    if (!device_is_ready(error_led.port)) {
        return -1;
    }

    // Configure LED GPIO pins as inactive outputs  
    ret = gpio_pin_configure_dt(&heartbeat_led, GPIO_OUTPUT_INACTIVE);
    if (ret < 0) {
        LOG_ERR("Failed to configure heartbeat LED (%d)", ret);
        return -1;
    }
    ret = gpio_pin_configure_dt(&action100_led, GPIO_OUTPUT_INACTIVE);
    if (ret < 0) {
        LOG_ERR("Failed to configure action100 LED (%d)", ret);
        return -1;
    }
    ret = gpio_pin_configure_dt(&action500_led, GPIO_OUTPUT_INACTIVE);
    if (ret < 0) {
        LOG_ERR("Failed to configure action500 LED (%d)", ret);
        return -1;
    }
    ret = gpio_pin_configure_dt(&error_led, GPIO_OUTPUT_INACTIVE);
    if (ret < 0) {
        LOG_ERR("Failed to configure error LED (%d)", ret);
        return -1;
    }

    // Configure the buttons
    ret = gpio_pin_configure_dt(&save, GPIO_INPUT);
    if (ret < 0) {
        LOG_ERR("Failed to configure save button (%d)", ret);
        return -1;
    }
    ret = gpio_pin_configure_dt(&send, GPIO_INPUT);
    if (ret < 0) {
        LOG_ERR("Failed to configure send button (%d)", ret);
        return -1;
    }

    // Check that the ADC0 interface is ready 
    if (!device_is_ready(vpp100.dev)) {
        LOG_ERR("ADC0 controller device(s) not ready");
        return -1;
    }
    // Configure the ADC channel 0
    int err_0; 
    err_0 = adc_channel_setup_dt(&vpp100);
    if (err_0 < 0) {
        LOG_ERR("Could not setup ADC0 channel (%d)", err_0);
        return err_0;
    };

    // Check that the ADC1 interface is ready 
    if (!device_is_ready(vpp500.dev)) {
        LOG_ERR("ADC1 controller device(s) not ready");
        return -1;
    }
    // Configure the ADC channel 1
    int err_1; 
    err_1 = adc_channel_setup_dt(&vpp500);
    if (err_1 < 0) {
        LOG_ERR("Could not setup ADC1 channel (%d)", err_1);
        return err_1;
    };

    // Check that the ADC2 interface is ready 
    if (!device_is_ready(battery.dev)) {
        LOG_ERR("ADC2 controller device(s) not ready");
        return -1;
    }
    // Configure the ADC channel 2
    int err_2; 
    err_2 = adc_channel_setup_dt(&battery);
    if (err_2 < 0) {
        LOG_ERR("Could not setup ADC2 channel (%d)", err_2);
        return err_2;
    };

    // Check that the 1st PWM controller is ready
    if (!device_is_ready(vpp100_brightness_control.dev)) {
        LOG_ERR("PWM device %s is not ready.", vpp100_brightness_control.dev->name);
        return -1;
    }

    // Check that the 2nd PWM controller is ready
    if (!device_is_ready(vpp500_brightness_control.dev)) {
        LOG_ERR("PWM device %s is not ready.", vpp500_brightness_control.dev->name);
        return -1;
    }

    
    // Configure the sensor 
    if (!device_is_ready(pressure_in)) {
        LOG_ERR("MPR pressure sensor %s is not ready", pressure_in->name);
        return;
    } else {
        LOG_INF("MPR pressure sensor %s is ready", pressure_in->name);
    }

    /* Initialize Bluetooth */
    int err = bluetooth_init(&bluetooth_callbacks, &remote_service_callbacks);
    if (err) {
        LOG_ERR("BT init failed (err = %d)", err);
    }

    
}

static void init_run(void *o)
{
    // Go to the read state 
    smf_set_state(SMF_CTX(&s_obj), &device_states[read]);
}

static void init_exit(void *o)
{
    //Start the heartbeat timer
    k_timer_start(&heartbeat_timer, K_MSEC(LED_ON_TIME_MS), K_MSEC(LED_ON_TIME_MS));

    //Start the battery timer
    k_timer_start(&battery_timer, K_MSEC(LED_ON_TIME_MS), K_MSEC(LED_ON_TIME_MS));

}

/*****************  READ STATE  *******************/ 
static void read_entry(void *o) 
{

    // start read timers 
    k_timer_start(&read_100hz_timer, K_MSEC(10), K_MSEC(10));
    k_timer_start(&read_500hz_timer, K_MSEC(2), K_MSEC(2));
}

static void read_run(void *o)
{
    k_msleep(100);
}

static void read_exit(void *o)
{
    // Stop the reading timers
    k_timer_stop(&read_100hz_timer);
    k_timer_stop(&read_500hz_timer);
    
    // Reset the sum and counters
    sum_0 = 0;
    sum_1 = 0;
    wave_counter_0 = 0;
    wave_counter_1 = 0;

}

/*****************  ACTION STATE  *******************/ 
static void action_entry(void *o)
{

    // Configure and add callbacks to button
    ret = gpio_pin_interrupt_configure_dt(&save, GPIO_INT_EDGE_TO_ACTIVE);
    gpio_init_callback(&save_cb, save_handler, BIT(save.pin));  
    gpio_add_callback(save.port, &save_cb);

    ret = gpio_pin_interrupt_configure_dt(&send, GPIO_INT_EDGE_TO_ACTIVE);
    gpio_init_callback(&send_cb, save_handler, BIT(send.pin));  
    gpio_add_callback(send.port, &send_cb);


    // Linear mapping of LED1
    LED1factor = (average_0)*(2.2)-11;
    brightness_100hz = MAX_BRIGHTNESS - (vpp100_brightness_control.period * LED1factor/100);
    ret = pwm_set_pulse_dt(&vpp100_brightness_control, brightness_100hz);

   LOG_INF("LED1Factor: %f", LED1factor);
   LOG_INF("brightness_100hz: %f", brightness_100hz);

   // Linear mapping of LED2
   LED2factor = (average_1)*(7)-71.5;
   brightness_500hz = MAX_BRIGHTNESS - (vpp500_brightness_control.period * LED2factor);
   pwm_set_pulse_dt(&vpp500_brightness_control, brightness_500hz);

   LOG_INF("LED2Factor: %f", LED2factor);
   LOG_INF("brightness_500hz: %f", brightness_500hz);
}

static void action_run(void *o)
{

    // Check for vbus and pressure
    if (usbregstatus == true || atm_pressure_kPa <= 0){
        // VBUS detected or no pressure (go to error)
        smf_set_state(SMF_CTX(&s_obj), &device_states[error]);
    }

}

static void action_exit(void *o)
{
    // Remove callbacks associated with the buttons
    gpio_remove_callback(save.port, &save_cb);
    gpio_remove_callback(send.port, &send_cb);
}

/*****************  ERROR STATE  *******************/
static void error_entry(void *o)
{;

    if (usbregstatus == true) {
        // VBUS detected, start error led timer (make led3 blink)
        k_timer_start(&error_timer, K_MSEC(LED_ON_TIME_MS), K_MSEC(LED_ON_TIME_MS));
        //k_timer_stop(&vbus_timer);
    }
    if (atm_pressure_kPa <= 0) {
        //  illuminate error led
        gpio_pin_set_dt(&error_led, 1);
    }
}

static void error_run(void *o)
{
    
    // if vbus off and pressure is detected go back to action
    if (usbregstatus == false && atm_pressure_kPa > 0) {
        smf_set_state(SMF_CTX(&s_obj), &device_states[action]);
    }
    

}

static void error_exit(void *o)
{
    // stop error led timer 
    k_timer_stop(&error_timer);

    // turn off error led 
    gpio_pin_set_dt(&error_led, 0); 

}

/*****************  CONNECT STATE  *******************/
static void connect_entry(void *o)
{

    // send a notification that "data" is ready to be read...
    int err = send_data_notification(current_conn, data_array, 1);
    if (err) {
        LOG_ERR("Could not send BT notification (err: %d)", err);
    }
    else {
        LOG_INF("BT data transmitted.");
    }

    /* Example of how to use the Battery Service
       ⁠ normalized_level ⁠ comes from an ADC reading of the battery voltage
       this function populates the BAS GATT with information
    */
    int normalized_level;
    err = bt_bas_set_battery_level((int)normalized_level);
    if (err) {
        LOG_ERR("BAS set error (err = %d)", err);
    }

    int battery_level;
    // this function retrieves BAS GATT with information
    battery_level =  bt_bas_get_battery_level();


}

static void connect_run(void *o)
{
    k_msleep(100);
}

static void connect_exit(void *o)
{
    LOG_INF("Leaving connect state");
}

/* Populate state table */
static const struct smf_state device_states[] = {
    [init] = SMF_CREATE_STATE(init_entry, init_run, init_exit),
    [read] = SMF_CREATE_STATE(read_entry, read_run, read_exit),
    [action] = SMF_CREATE_STATE(action_entry, action_run, action_exit),
    [error] = SMF_CREATE_STATE(error_entry, error_run, error_exit),
    [connect] = SMF_CREATE_STATE(connect_entry, connect_run, connect_exit),
};

void main(void)
{
    int32_t ret;

    smf_set_initial(SMF_CTX(&s_obj), &device_states[init]); 

    while (1) 
    {
        /* State machine terminates if a non-zero value is returned */
        ret = smf_run_state(SMF_CTX(&s_obj));
        if (ret) {
            /* handle return code and terminate state machine */
            break;
        }

        // Read data from the pressure sensor 
        //atm_pressure_kPa = read_pressure_sensor(pressure_in, OVERSAMPLE, 0);
        //LOG_INF("PRESSURE (%d)", atm_pressure_kPa);

        // Check for VBUS
        usbregstatus = nrf_power_usbregstatus_vbusdet_get(NRF_POWER);
        //LOG_INF("VBUS status (%d)", usbregstatus);

        k_msleep(MEASUREMENT_DELAY_MS); 
    }
}
